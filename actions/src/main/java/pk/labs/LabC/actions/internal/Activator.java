/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;
import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;
/**
 *
 * @author Bartek
 */
public class Activator implements BundleActivator {
    
    private static BundleContext Context;

    @Override
    public void start(BundleContext bc) {

        Context = bc;

        AnimalAction zwierz1 = new AnimalAction() {
            public String toString() {
                return "Jedz";
            }

 

            public boolean execute(Animal animal) {
                animal.setStatus("Jedz");
                return true;
            }
        };

        AnimalAction zwierz2 = new AnimalAction() {
            public String toString() {
                return "Szczekaj";
            }

            public boolean execute(Animal animal) {
                animal.setStatus("szczekaj");
                return true;
            }
        };

        AnimalAction zwierz3 = new AnimalAction() {
            public String toString() {
                return "Mrucz";
            }

            public boolean execute(Animal animal) {
                animal.setStatus("Mrucz");
                return true;
            }
        };

        Hashtable propa1 = new Hashtable();
        propa1.put("species", "Cygan");
        Hashtable propa2 = new Hashtable();
        propa2.put("species", new String[]{"Pies"});
        Hashtable propa3 = new Hashtable();
        propa3.put("species", "Kot");
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), zwierz1, propa1);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), zwierz2, propa2);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), zwierz3, propa3);

    }
    
    @Override
    public void stop(BundleContext bc) {
        Context = null;
    }
}
