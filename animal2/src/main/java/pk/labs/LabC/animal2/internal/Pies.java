/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal2.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;
/**
 *
 * @author Bartek
 */
public class Pies implements Animal{
    private String status;
    private static PropertyChangeSupport listener;
    
    @Override
    public String getSpecies() {
        return "Pies";
    }

    @Override
    public String getName() {
        return "Gruzołap";
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
