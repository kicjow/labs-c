/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal3.internal;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;


public class Activator implements BundleActivator{
    
    private BundleContext Context;

    @Override
    public void start(BundleContext bc) throws Exception {
        Context = bc;
        Context.registerService(Animal.class.getName(), new Kot(), null);
        pk.labs.LabC.logger.Logger.get().log(this, "Kot po płocie posuwa");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        Context = null;
        pk.labs.LabC.logger.Logger.get().log(this, "Kot pod płotem przemyka");
    }
    
}
